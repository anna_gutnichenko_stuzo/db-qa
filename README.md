#README

##What is this repository for?
End to End tests for Dave & Busters

##How do I get set up?

1. Clone project ``` git clone https://anna_gutnichenko_stuzo@bitbucket.org/anna_gutnichenko_stuzo/db-qa.git ```
2. Run ```bundle exec cucumber --tags @<bunch_of_tests> DEVICE_NAME='<device_to_run_tests_on>'```

###Smoke for IOS simulator:
```bundle exec cucumber --tags @ios_smoke DEVICE_NAME='ios_simulator'```

###Smoke for Android simulator:
```bundle exec cucumber --tags @android_smoke DEVICE_NAME='android_simulator'```

###Smoke for IOS real device:
```bundle exec cucumber --tags @ios_smoke DEVICE_NAME='ios_real'```

###Smoke for Android real device:
```bundle exec cucumber --tags @android_smoke DEVICE_NAME='android_real'```


