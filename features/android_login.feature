@android_login
@android
@android_smoke
Feature: Check signin cases

  Scenario: Signin with valid mobile phone number
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    Then I click Logout button. Android


#  Scenario: Login with not registered phone
#  Scenario: Login with invalid phone