@ios_security_prompt
@ios
@ios_smoke
Feature: Check security promt events

  Scenario: Check that user can setup valid passcode
    Given I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Account button. IOS
    Then I click PaymentMethods button. IOS
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1235           |
#    And I click TouchId button. IOS
#    Then I click OkTouchId button. IOS
    And PaymentMethods block should contain "PAYMENT METHODS" text. IOS
    Then I click Home button. IOS
    And I click Logout button. IOS

  Scenario: Check that user can not use too weak password
    Given I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Account button. IOS
    Then I click PaymentMethods button. IOS
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1111           |
      | 1234           |
      | 4321           |
      | 666            |
    And PasscodeError block should contain "Your passcode is too weak. Please try another one." text. IOS
    Then I click Crosslet button. IOS
    Then I click Home button. IOS
    And I click Logout button. IOS

  Scenario: Check that user can not enter invalid passcode on security prompt of already set passcode
    Given I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Account button. IOS
    Then I click PaymentMethods button. IOS
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1234           |
    And PasscodeDoesNotMatch block should contain "Passcode does not match. Please try again." text. IOS
    Then I click Crosslet button. IOS
    Then I click Home button. IOS
    And I click Logout button. IOS

  Scenario: Check that user can reset passcode
    Given I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Account button. IOS
    Then I click PaymentMethods button. IOS
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1235           |
    And PaymentMethods block should contain "PAYMENT METHODS" text. IOS
    Then I click Back button. IOS
    Then I click PaymentMethods button. IOS
    And I click ResetPasscode button. IOS
    And I click ConfirmResetPasscode button. IOS
    Then I make explicit wait for 2 seconds. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1236           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. IOS
    Then I fill in fields with following data:. IOS
      | passcode field |
      | 1236           |
    Then I click UpdatePayments button. IOS
    And PaymentMethods block should contain "PAYMENT METHODS" text. IOS
    Then I click Back button. IOS
    Then I click Home button. IOS
    And I click Logout button. IOS
