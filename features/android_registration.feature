@android_registration
@android
@android_smoke
Feature: Check signup cases

  Scenario: Signup with valid email and login with it
    Given I fill in fields with following data:. Android
      | login signup field |
      | 6666666666         |
    Then I click CreateAccount button. Android
    And I fill in fields with following data:. Android
      | otp field |
      | 666666    |
    Then I fill in fields with following data:. Android
      | email field    |
      | dummy@email.ua |
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    And I click DB button. Android
    Then I click Ok button. Android
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    Then I click AgreeContinue button. Android

  Scenario: Check that user can not register with primary store not selected

#  Scenario: Check that user is not allowed to signup with already registered phone
#
#  Scenario: Check that user is not allowed to Signup with invalid phone
#
#  Scenario: Check that user is not allowed to signup with TermsConditions switched off
#    #And I click SwitchAgreeTermsConditions button. Android
#
#  Scenario: Check that user Allowed to signup with ReceiveMarketingEmail switched off
