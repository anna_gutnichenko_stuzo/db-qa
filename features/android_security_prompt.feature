@android_security_prompt
@android
@android_smoke
Feature: Check security promt events

  Scenario: Check that user can setup valid passcode
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Account button. Android
    Then I click PaymentMethods button. Android
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1235           |
    And PaymentMethods block should contain "PAYMENT METHODS" text. Android
    Then I click Home button. Android
    And I click Logout button. Android

  Scenario: Check that user can not use too weak password
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Account button. Android
    Then I click PaymentMethods button. Android
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1111           |
      | 1234           |
      | 4321           |
      | 666            |
    And PasscodeError block should contain "Your Passcode is weak" text. Android
    Then I click Crosslet button. Android
    Then I click Home button. Android
    And I click Logout button. Android

  Scenario: Check that user can not enter invalid passcode on security prompt of already set passcode
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Account button. Android
    Then I click PaymentMethods button. Android
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1234           |
    And PasscodeError block should contain "Passcode does not match. Please try again." text. Android
    Then I click Crosslet button. Android
    Then I click Home button. Android
    And I click Logout button. Android

  Scenario: Check that user can reset passcode
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Account button. Android
    Then I click PaymentMethods button. Android
    Then CreatePasscode block should contain "CREATE A 4-DIGIT PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1235           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1235           |
    And PaymentMethods block should contain "PAYMENT METHODS" text. Android
    Then I click Back button. Android
    Then I click PaymentMethods button. Android
    And I click ResetPasscode button. Android
    And I click ConfirmResetPasscode button. Android
    Then I make explicit wait for 2 seconds. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1236           |
    Then VerifyPasscode block should contain "VERIFY YOUR PASSCODE" text. Android
    Then I fill in fields with following data:. Android
      | passcode field |
      | 1236           |
    Then I click UpdatePayments button. Android
    And PaymentMethods block should contain "PAYMENT METHODS" text. Android
    Then I click Back button. Android
    Then I click Home button. Android
    And I click Logout button. Android
