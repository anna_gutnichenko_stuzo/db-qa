@android_cards
@android
@android_smoke
Feature: Check payment cards cases

  Scenario: Check that user can get Digital power card
    #And I click SignIn button. Android
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Cards button. Android
    Then I click Recharge button. Android
    Then Boost block should contain "GO! BOOST YOUR PLAY!" text. Android
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    Then I click ViewAllRates button. Android
    And I scroll "2" times to the point: "1037", "1053", "1037", "346". Android
    Then I click NNN button. Android
    Then AddVR block should contain "ADD VR ATTRACTIONS" text. Android
    And I click AddVR button. Android

  Scenario: Check that user can get physical power card
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Cards button. Android
    Then I click PhysicalPowerCard button. Android
    Then I fill in fields with following data:. Android
      | power card number field | pin_field |
      | 4111111111111111        | 1234      |
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    And I click ConfirmPhysicalCard button. Android
    Then I click Continue button. Android
    And I click Home button. Android
    Then I click Logout button. Android

  Scenario: Check that user can edit his physical power card
    Then I fill in fields with following data:. Android
      | login field |
      | 5555555555  |
    And I make explicit wait for 2 seconds. Android
    And I click Confirm button. Android
    Then I fill in fields with following data:. Android
      | otp field |
      | 555555    |
    And I click Cards button. Android
    Then I click PhysicalPowerCard button. Android
    Then I fill in fields with following data:. Android
      | power card number field | pin_field |
      | 4111111111111111        | 1234      |
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    And I click ConfirmPhysicalCard button. Android
    Then I click Continue button. Android
    And I make explicit wait for 3 seconds. Android
    And I click CardArrow button. Android
    Then I fill in fields with following data:. Android
      | pc nickname field |
      | anna_gutnichenko  |
    #this step should be removed when bug with save is fixed
    And I click Style button. Android
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". Android
    And I click ConfirmSave button. Android
    Then NickName block should contain "anna_gutnichenko" text. Android
    And I click Home button. Android
    Then I click Logout button. Android
    