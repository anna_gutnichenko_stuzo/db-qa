require 'bundler'
Bundler.require(:default)

require_relative '../../features/pages/android_main'
require_relative '../../features/pages/ios_main'

#----------------CAPS FOR iOS real---------------------------------#
caps1 = {
    platformName: 'iOS',
    platformVersion: '10.2.1',
    deviceName: 'iPhone_test',
    app: '',
    udid: 'e91a4feca68324e3ae1932b98adfb35bc7ea8ce8',
    showXcodeLog: true,
    xcodeOrgId: 'SSPC3KX2X2',
    xcodeSigningId: 'iPhone Developer',
    automationName: 'XCUITest'
}

#----------------CAPS FOR iOS simulator-------------------------------#
caps2 = {
    platformName: 'iOS',
    platformVersion: '12.0',
    deviceName: 'iPhone 8',
    app: (File.join(File.dirname(__FILE__), 'db.app')),
    automationName: 'XCUITest'
}

#----------------CAPS FOR Android real---------------------------------#
caps3 =
    {
        platformName: 'Android',
        deviceName: 'Galaxy S9',
        platformVersion: '8.0.0',
        app: (File.join(File.dirname(__FILE__), 'db_real.apk')),
        newCommandTimeout: '60',
        autoAcceptAlerts: true
    }

#----------------CAPS FOR Android simulator---------------------------------#
caps4 =
    {
        platformName: 'Android',
        deviceName: 'Nexus 5x',
        platformVersion: '7.0',
        app: (File.join(File.dirname(__FILE__), 'db.apk')),
        avd: 'Nexus_5X_API_24',
        #automationname: 'uiautomator2'
        #appPackage: 'com.dave.and.buster',
        #appActivity: 'dave.and.busters.registration.mvvm.view.activity.OnBoardingActivity'
    }

#----------------CAPS FOR Android Sauce---------------------------------#
caps5 = {
    platformName: 'iOS',
    platformVersion: '12.0',
    deviceName: 'iPhone 8',
    app: 'sauce-storage:db.zip',
    automationName: 'XCUITest',
    appiumVersion: '1.9.1'
}

#----------------CAPS FOR IOS Sauce---------------------------------#
caps6 =
    {
        platformName: 'Android',
        deviceName: 'Samsung Galaxy S7 GoogleAPI Emulator',
        platformVersion: '7.0',
        app: 'sauce-storage:db.apk',
        appiumVersion: '1.9.1',
        browserName: '',
        name: 'Ruby Appium Sauce example'
    }

USERNAME = 'quo_vadis'
ACCESS_KEY = '251634a3-2a9c-4c21-9955-896a71919761'

case ENV['DEVICE_NAME']
when 'ios_real'
  caps = caps1
  url =  'http://localhost:4723/wd/hub'
when 'ios_simulator'
  caps = caps2
  url =  'http://localhost:4723/wd/hub'
when 'android_real'
  caps = caps3
  url =  'http://localhost:4723/wd/hub'
when 'android_simulator'
  caps = caps4
  url =  'http://localhost:4723/wd/hub'
when 'ios_sauce'
  caps = caps5
  url = "http://#{USERNAME}:#{ACCESS_KEY}@ondemand.saucelabs.com:80/wd/hub"
when 'android_sauce'
  caps = caps6
  url = "http://#{USERNAME}:#{ACCESS_KEY}@ondemand.saucelabs.com:80/wd/hub"
end

Capybara.register_driver(:appium) do |app|
  appium_lib_options = {
      server_url: url
  }
  all_options = {
      appium_lib: appium_lib_options,
      caps: caps,
      global_driver: true
  }
  Appium::Capybara::Driver.new app, all_options

end

Capybara.default_driver = :appium
Capybara.server_host = '0.0.0.0'
Capybara.default_selector = :xpath
Capybara.ignore_hidden_elements = true
Capybara.server_port = 56844
