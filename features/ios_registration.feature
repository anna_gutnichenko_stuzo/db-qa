@ios_registration
@ios
@ios_smoke
Feature: Check signup cases

  Scenario: Signup with valid phone and login with it
    Given I click CreateAccount0 button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555666666  |
    #Then I click CreateAccount1 button. IOS
    And I click Confirm button. IOS
    And I fill in fields with following data:. IOS
      | otp field |
      | 666666    |
    And I make explicit wait for 4 seconds. IOS
    Then I click SelectPrimaryStore button. IOS
    And I click LocatePermission button. IOS
    Then I click AllowLocation button. IOS
    And I click Back button. IOS
    Then I fill in fields with following data:. IOS
      | email field    |
      | dummy@email.ua |
    And I click DB button. IOS
    Then I click OkDB button. IOS
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". IOS
    Then I click AgreeContinue button. IOS
    Then AccountCreated block should contain "Your account was created. You can purchase your Power Card next." text. IOS
    And I click Continue button. IOS
    Then I click Logout button. IOS

  Scenario: Check that user can not register with primary store not selected

#    Scenario: Check that user is not allowed to signup with already registered phone
#    Scenario: Check that user is not allowed to Signup with invalid phone
