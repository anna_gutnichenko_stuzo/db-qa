@ios_cards
@ios
@ios_smoke
Feature: Check power cards cases

  @i
  Scenario: Check that user can get Digital power card
    And I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I make explicit wait for 2 seconds. IOS
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Cards button. IOS
    Then I click DigitalPowerCard button. IOS
    Then Boost block should contain "GO! BOOST YOUR PLAY!" text. IOS
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". IOS
    Then I make explicit wait for 5 seconds. IOS
    Then I click ViewAllRates button. IOS
    And I scroll "2" times to the point: "1037", "1053", "1037", "346". IOS
    Then I click NNN button. IOS
    And I click Charge button. IOS
    And I click Home button. IOS
    Then I click Logout button. IOS

  Scenario: Check that user can get Physical power card
    And I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I make explicit wait for 2 seconds. IOS
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Cards button. IOS
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". IOS
    Then I click PhysicalPowerCard button0. IOS
    Then I click PhysicalPowerCard button1. IOS
    And PhysicalPC block should contain "ADD YOUR PHYSICAL POWER CARD" text. IOS
    Then I fill in fields with following data:. IOS
      | power card number field | pin_field |
      | 4                       | 12345     |
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". IOS
    And I click ConfirmPhysicalCard button. IOS
    And Congratulations block should contain "Your Power Card was successfully added." text. IOS
    Then I click Continue button. IOS
    And I click Home button. IOS
    Then I click Logout button. IOS

  Scenario: Check that user can edit his Physical power card
    And I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I make explicit wait for 2 seconds. IOS
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    And I click Cards button. IOS
    And I click CardArrow button. IOS
    Then I fill in fields with following data:. IOS
      | pc nickname field |
      | TEST              |
    And I scroll "1" times to the point: "1037", "1053", "1037", "546". IOS
    And I click ConfirmSave button. IOS
    Then I make explicit wait for 3 seconds. IOS
    #currently this step is commented cause JOSHTEST value is hardcoded
    #Then NickName block should contain "JOSHTEST" text. IOS
    And I click Home button. IOS
    Then I click Logout button. IOS
