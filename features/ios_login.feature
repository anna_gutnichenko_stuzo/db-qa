@ios_login
@ios
@ios_smoke
Feature: Check signin cases

  Scenario: Signin with valid mobile phone number
    And I click SignIn button. IOS
    Then I fill in fields with following data:. IOS
      | login field |
      | 5555111111  |
    And I click Confirm button. IOS
    Then I fill in fields with following data:. IOS
      | otp field |
      | 111111    |
    Then WelcomeToDb block should contain "WELCOME TO D&B" text. IOS
    And I click Logout button. IOS

#  Scenario: Login with not registered email
#  Scenario: Login with invalid email
#  Scenario: Login with not registered phone
