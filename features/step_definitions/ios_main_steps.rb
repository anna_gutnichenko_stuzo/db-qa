
And(/^I click ([\w ]+). IOS$/) do |object|
  @main = IOSMain.new
  object = object.gsub(' ', '_').downcase
  wait = Selenium::WebDriver::Wait.new(:timeout => 50)
  wait.until {@main.send(object.to_sym).visible?}
  @main.send(object.to_sym).click
end

Then(/^I fill in fields with following data:. IOS$/) do |table|
  @main = IOSMain.new
  data_hash = table.hashes[0]
  data_hash.each do |selector, input_data|
    raw_selector = selector.gsub(' ', '_').downcase.to_sym
    wait = Selenium::WebDriver::Wait.new(:timeout => 1500)
    wait.until {@main.send(raw_selector.to_sym).visible?}
    @main.send(raw_selector).native.send_keys(input_data)
  end
end

Then(/^([\w ]+) should contain "([^"]*)" text. IOS$/) do |block, expected_text|
  @main = IOSMain.new
  block = block.gsub(' ', '_').downcase.to_sym
  wait = Selenium::WebDriver::Wait.new(:timeout => 1500)
  wait.until {@main.send(block.to_sym).visible?}
  expect(@main.send(block).text).to include expected_text
end

And (/^I scroll "([^"]*)" times to the point: "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)". IOS$/) do |param, x1, y1, x2, y2|
  param.to_i.times do
    Appium::TouchAction.new.swipe(start_x: x1.to_i, start_y: y1.to_i, end_x: x2.to_i, end_y: y2.to_i, duration: 400).perform
  end
end

And (/^I make explicit wait for ([\d ]+) seconds. IOS$/) do |n_seconds|
  sleep n_seconds.to_i
end
